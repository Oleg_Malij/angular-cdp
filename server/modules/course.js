/**
 * Created by Oleg on 28/6/2016.
 */
var mongoose = require('mongoose');

var CourseSchema = new mongoose.Schema({
    title: {type: String, required: true},
    description: {type: String, required: true},
    date: {type: String, required: true},
    duration: {type: Number, required: true},
    authors: []
});

var CourseModel = mongoose.model('Course', CourseSchema, 'Course');

module.exports = {
    add: (course) => {
        'use strict';
        return new Promise((fullfil, reject) => {
            var courseModel = new CourseModel(course);
            courseModel.save((err, course) => {
                if(err) {
                    return reject(Error(err.message));
                }
                fullfil(course);
            });
        });
        
    },
    update: (course) => {
        'use strict';
        let id = course._id;
        delete course._id;
        return new Promise((fulfill, reject) => {
            CourseModel.findByIdAndUpdate(id, course, {new: true}, (err, course) => {
                if(err) {
                    return reject(Error(err.message));
                }

                fulfill(course);
            });
        });
        
    },
    get: (id) => {
        'use strict';
        return findCourse({_id: id});
    },
    getAll: () => {
        'use strict';
        return findCourse();
    },
    remove: (id) => {
        'use strict';
        return new Promise((fullfil, reject) => {
            CourseModel.remove({_id: id}, function(err) {
                if(err) {
                    return reject(Error(err.message));
                }

                fullfil();
            });
        });
    }
};

function findCourse(find) {
    'use strict';
    find = find || {};
    return new Promise((fullfil, reject) => {
        CourseModel.find(find, (err, courses) => {
            if(err) {
                return reject(Error(err.message));
            }
            fullfil(courses);
        });
    });
}