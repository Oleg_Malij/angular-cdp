const cleanPlugin = require('clean-webpack-plugin');
const ngAnnotatePlugin = require('ng-annotate-webpack-plugin');
const webpack = require('webpack');
const path = require('path');

module.exports = {
    context: path.join(__dirname,'..', 'app'),
    entry: './app.js',
    output: {
        filename: 'main.js',
        path: __dirname + '/public/javascripts'
    },
    watch: true,
    // Add this property to run watcher with vagrant
    watchOptions: {
        poll: true
    },
    resolve: {
        alias: {
            'npm': __dirname + '/node_modules'
        }
    },
    resolveLoader: {
        root: __dirname + '/node_modules'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['babel-preset-es2015'].map(require.resolve)
                }
            },
            { test: /\.css$/, loader: 'style-loader!css-loader'}/*,
            {
                test: /\.html$/,
                loader: 'ngtemplate?relativeTo=' + path.resolve(__dirname, '../app') + '/!html'
            }*/
        ]
    },
    plugins: [
        new cleanPlugin(['dist']),
        new ngAnnotatePlugin({
            add: true
        }),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: false,
            mangle: false,
            compress: {
                warnings: true
            }
        })
    ]
};