/**
 * Created by Oleg on 28/6/2016.
 */
var express = require('express');
var router = express.Router();
var CourseModel = require('../modules/course');

/* GET users listing. */
router.post('/course', (req, res, next) => {
    'use strict';
    res.set('Content-Type', 'application/json');
    CourseModel.add(req.body).then((course) => {
        res.status(200).send({done: true, data: course.toObject()});
    }).catch((error) => {
        res.status(500).send({done: false, error: error});
    });
});

router.post('/course', function (req, res, next) {
    'use strict';
    res.set('Content-Type', 'application/json');
    CourseModel.add(req.body).then((course) => {
        res.status(200).send({done: true, data: course.toObject()});
    }).catch((error) => {
        res.status(500).send({done: false, error: error});
    });
});

router.get('/course/:id?', (req, res, next) => {
    'use strict';
    res.set('Content-Type', 'application/json');
    let courseData = null;

    if(!req.params.id) {
        courseData = CourseModel.getAll();
    } else {
        courseData = CourseModel.get(req.params.id);
    }

    courseData.then((course) => {
        var data = (!req.params.id) ? [] : {};

        if(course.length && !req.params.id) {
            course.reduce((arr, current) => {
                arr.push(current);
                return arr;
            }, data);
        }else {
            if (course) {
                data = course.shift();
            }
        }

        res.status(200).send(data);
    }).catch((error) => {
        res.status(500).send({done: false, error: error.message});
    });
});

router.delete('/course/:id', (req, res, next) => {
    'use strict';
    res.set('Content-Type', 'application/json');
    CourseModel.remove(req.params.id).then(() => {
        res.status(200).send({});
    }).catch((error) => {
        res.status(500).send({done: false, error: error.message});
    });
});

router.put('/course/:id', (req, res, next) => {
    'use strict';
    res.set('Content-Type', 'application/json');
    CourseModel.update(req.body).then((course) => {
        res.status(200).send(course);
    }).catch((error) => {
        res.status(500).send({done: false, error: error.message});
    });
});

module.exports = router;