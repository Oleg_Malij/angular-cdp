var express = require('express');
var router = express.Router();

/* GET users listing. */
router.post('/login', function (req, res, next) {
    var loginResponse = {valid: false};

    if(req.body['login'] === 'test' && req.body['password'] === 'test') {
        loginResponse.valid = true;
    }

    res.send(loginResponse);
});

module.exports = router;
