/**
 * Created by Oleg on 26/6/2016.
 */
class CourseController {
    /*@ngInject*/
    constructor($state, courseResource, modalService, authorsService) {
        this.$state = $state;
        this.courseResource = courseResource;
        this.courses = [];
        this.filter = '';
        this.modalService = modalService;
        this.authorsService = authorsService;
        this.courseFilter = '';
    }

    $onChanges(changes) {
        if (changes.courseData) {
            this.courses = this.courseData.slice();
            replaceAuthors(this.courses, this.authorsService);
        }
    }
    // Define methods
    applyFilter() {
        this.courseFilter = this.filter;
    }

    addCourse() {
        this.$state.go('courseNew', {id: 'new'});
    }

    removeCourse(id) {
        let modalOptions = {
            resolve: {
                params: () => {
                    return {
                        title: 'Delete course',
                        text: 'Are you sure?',
                        yesButton: 'Yes',
                        noButton: 'No'
                    };
                }
            }
        };
        this.modalService.openModal(modalOptions).then(() => {
            this.courseResource.remove(id).$promise.then(() => {
                this.courseResource.getAll().$promise.then((response) => {
                    this.courses = response.slice();
                    replaceAuthors(this.courses, this.authorsService);
                });
            });
        }, ()=>'');
    }
}

function replaceAuthors(courses, authorsService) {
    'use strict';
    for(let i = 0;  i < courses.length; i++) {
        let autors = [].concat(courses[i].authors);
        courses[i].authors.length = 0;
        authorsService.processAuthors(authorsService.get(), courses[i].authors, autors);
    }
}

export default CourseController;