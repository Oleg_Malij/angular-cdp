/**
 * Created by Oleg on 26/6/2016.
 */
import CourseController from './course.controller';

const CourseComponent = {
    controller: CourseController,
    templateUrl: require('!ngtemplate?requireAngular!html!./course.html'),
    bindings: {
        courseData: '<'
    }
};

export default CourseComponent;