/**
 * Created by Oleg on 26/6/2016.
 */

import angular from 'npm/angular';
import CourseComponent from './course.component';
import AddCourseComponent from './courses-edit';
import CourseResource from './course.service';
/*@ngInject*/
const course = angular.module('course', [AddCourseComponent])
    .component('courses', CourseComponent)
    .service('courseResource', CourseResource)
    .name;

export default course;