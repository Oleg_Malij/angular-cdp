/**
 * Created by Oleg on 28/6/2016.
 */
class CourseService {
    /*@ngInject*/
    constructor($resource) {
        this.resource = $resource('/api/course/:id', {id: '@id'}, {'update': {method: 'PUT'}});
        this.coursesData = [];
    }

    add(course) {
        return this.resource.save(course);
    }

    getAll() {
        return this.resource.query((data) => {
            this.coursesData = JSON.parse(angular.toJson(data));
        });
    }

    get(id) {
        return this.resource.get({id: id}, (data) => {
            this.coursesData.length = 0;
            this.coursesData.push(data.toJSON());
        });
    }

    remove(id) {
        return this.resource.remove({id: id});
    }

    update(course) {
        return this.resource.update({id: course._id}, course);
    }

    getCourses() {
        return this.coursesData;
    }

}

export default CourseService;