/**
 * Created by Oleg on 28/6/2016.
 */
'use strict';

import angular from 'npm/angular';
import CourseEditComponent from './course-edit.component';
import CourseService from '../course.service';
import DurationDirective from './duration.directive';
import DatePatternDirective from './date.pattern.directive';
/*@ngInject*/
const courseEdit = angular.module('courseEdit', [])
    .component('courseEdit', CourseEditComponent)
    .service('courseService', CourseService)
    .directive('duration', () => new DurationDirective())
    .directive('datePattern', () => new DatePatternDirective())
    .name;

export default courseEdit;