/**
 * Created by Oleg on 17/7/2016.
 */
'use strict';
/*@ngInject*/
const DurationDirective = () => ({
    template: '<h5>{{duration}}</h5>',
    restrict: 'A',
    scope: {
        duration: '@'
    },
     link($scope, $element, $attrs) {
         $scope.$watch('duration', () => {
             let minutes = $attrs.duration % 60;
             let hours = ($attrs.duration - minutes) / 60;
             let hWOrd = 'Hour';
             let h = hours % 10;

             if (h >= 2) {
                 hWOrd = 'Hours';
             }

             $scope.duration = hours + ' ' + hWOrd + ' ' + minutes + ' min.';
         });
     }
});
/*@ngInject*/
export default DurationDirective;