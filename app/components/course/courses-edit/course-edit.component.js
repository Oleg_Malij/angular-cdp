/**
 * Created by Oleg on 28/6/2016.
 */
import CourseEditController from './course-edit.controller';

const CourseEditComponent = {
    controller: CourseEditController,
    templateUrl: require('!ngtemplate?requireAngular!html!./course-edit.html'),
    bindings: {
        courseData: '<'
    }
};

export default CourseEditComponent;