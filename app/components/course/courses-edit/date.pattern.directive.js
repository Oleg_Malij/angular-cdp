/**
 * Created by Oleg on 25/7/2016.
 */
'use strict';
/*@ngInject*/

const DatePatternDirective = () => ({
    template: '<input type="text" class="form-control" id="date" ng-keydown="keyDown($event)" ng-change="changeDate()" required name="date" ng-model="date">',
    require: 'ngModel',
    restrict: 'E',
    scope: {
        ngModel: '='
    },
    link(scope, element, attrs, ctrl) {

        scope.keyDown = ($event) => {
            var arrowDeleteBackKeys = ($event.which === 46 || $event.which === 8 || $event.which === 37 || $event.which === 39);

            if($event.key !== attrs.delimiter && isNaN($event.key) && !arrowDeleteBackKeys) {
                $event.preventDefault();
            }

            var escapedPattern = attrs.mask.replace(/[-[\]{}()*+?.,\\^$|#\s\\d\\w]/g, '\\$&');
            var regExp = new RegExp('^' + escapedPattern.substr(0, ($event.target.value.length + 1) * 2) + '$');

            if(!regExp.test($event.target.value + $event.key) && !arrowDeleteBackKeys) {
                $event.preventDefault();
            }
        };

        scope.changeDate = () => {
            scope.ngModel = scope.date;
        };

        ctrl.$render = () => scope.date = ctrl.$modelValue;
    }

});
/*@ngInject*/
export default DatePatternDirective;