/**
 * Created by Oleg on 28/6/2016.
 */
class CourseEditController{
    /*@ngInject*/
    constructor($scope, $state, courseResource, modalService, authorsService) {
        this.$scope = $scope;
        this.$state = $state;
        this.courseResource = courseResource;
        this.modalService = modalService;
        this.authorsService = authorsService;
        this.course = {};
        this.authorsSelectedList = [];
    }

    $onChanges(changes) {
        this.authorsList = this.authorsService.get();
        if (changes.courseData && this.courseData._id) {
            this.course = this.courseData.toJSON();
            this.authorsService.processAuthors(this.authorsList, this.authorsSelectedList, this.course.authors);
        }
    }
    // Define methods
    save() {
        this.course.authors = this.authorsSelectedList.map((item) => {
            return item.id;
        });

        if(!this.$scope.course.$valid || !this.course.authors.length) {
            let modalOptions = {
                resolve: {
                    params: () => {
                        return {
                            title: 'Warning!',
                            text: 'Invalid data!',
                            noButton: 'Close'
                        };
                    }
                }
            };
            this.modalService.openModal(modalOptions);
            return;
        }

        if(this.course._id) {
            this.courseResource.update(this.course).$promise.then(() => {
                this.$state.go('courses');
            });
        }else {
            this.courseResource.add(this.course).$promise.then(() => {
                this.$state.go('courses');
            });
        }
    }

    cancel() {
        this.$state.go('courses');
    }

    filterDigits($event) {
        var arrowDeleteBackKeys = ($event.which === 46 || $event.which === 8 || $event.which === 37 || $event.which === 39);
            if(($event.which < 48 || $event.which > 57) && ($event.which < 96 || $event.which > 105) && !arrowDeleteBackKeys) {
                $event.preventDefault();
            }
    }

    addAuthor() {
        if(!this.selectedAuthor || !this.selectedAuthor.length) {
            this.selectedAuthor = [this.authorsList[0]];
        }
        this.authorsService.processAuthors(this.authorsList, this.authorsSelectedList, this.selectedAuthor);
    }

    removeAuthor() {
        if(!this.authors || !this.authors.length) {
            this.authors = [this.authorsSelectedList[0]];
        }
        this.authorsService.processAuthors(this.authorsSelectedList, this.authorsList, this.authors);
    }
}

export default CourseEditController;