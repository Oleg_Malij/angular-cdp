/**
 * Created by Oleg on 28/6/2016.
 */

import angular from 'npm/angular';
import LogoutBoxComponent from './logout-box.component';
/*@ngInject*/
const logoutBox = angular.module('logoutBox', [])
    .component('logoutBox', LogoutBoxComponent)
    .name;

export default logoutBox;