/**
 * Created by Oleg on 28/6/2016.
 */

import LogoutBoxController from './logout-box.controller';

const LogoutBoxComponent = {
    controller: LogoutBoxController,
    templateUrl: require('!ngtemplate?requireAngular!html!./logout-box.html')
};

export default LogoutBoxComponent;