/**
 * Created by Oleg on 28/6/2016.
 */

class LogoutBoxController {
    /*@ngInject*/
    constructor($state, loginService) {
        this.$state = $state;
        this.loginService = loginService;
        this.loginStatus = loginService.status;
    }
    // Define methods
    logOut() {
        this.loginService.setLoginStatus();
        this.$state.go('login');
    }
}

export default LogoutBoxController;