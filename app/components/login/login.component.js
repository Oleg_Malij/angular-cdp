/**
 * Created by Oleg on 26/6/2016.
 */

import LoginController from './login.controller';
/*@ngInject*/
const LoginComponent = {
    controller: LoginController,
    templateUrl: require('!ngtemplate?requireAngular!html!./login.html')
};

export default LoginComponent;