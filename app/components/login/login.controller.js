/**
 * Created by Oleg on 18/6/2016.
 */
class LoginController {
    /*@ngInject*/
    constructor($state, loginService) {
        this.message = false;
        this.loginService = loginService;
        this.$state = $state;
    }
    // Define methods
    logIn() {
        this.loginService.processLogin(this.login, this.password).then((response) => {
            if(response.data.valid) {
                this.message = false;
                this.loginService.setLoginStatus(this.login);
                this.$state.go('courses');
            }else {
                this.message = 'Wrong Login or Password';
                this.password = '';
            }
        });
    }
}

export default LoginController;