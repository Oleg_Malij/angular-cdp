/**
 * Created by Oleg on 26/6/2016.
 */
'use strict';
import angular from 'npm/angular';
import LoginComponent from './login.component';
import LoginService from './login.service';
import LogoutBoxComponent from './logout-box';
/*@ngInject*/
const login = angular.module('login', [LogoutBoxComponent])
    .component('login', LoginComponent)
    .service('loginService', LoginService)
    .name;

export default login;