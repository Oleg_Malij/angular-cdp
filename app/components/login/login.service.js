/**
 * Created by Oleg_Malii on 6/27/2016.
 */

class LoginService {
    /*@ngInject*/
    constructor($http) {
        this.$http = $http;
        this.status = {};
    }

    processLogin(login, password) {
        return this.$http({
            method: 'POST',
            url: '/user/login',
            data: {
                login: login,
                password: password
            },
            responseType: 'json'
        });
    }

    getLoginStatus() {
        return localStorage.getItem('login');
    }
    
    setLoginStatus(status) {
        if(!status) {
            delete this.status['login'];
            localStorage.clear();
            return;
        }
        this.status['login'] = status;
        localStorage.setItem('login', status);
    }
}

export default LoginService;