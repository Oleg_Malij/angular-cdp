/**
 * Created by Oleg on 24/7/2016.
 */
class BreadcrumbsController {
    /*@ngInject*/
    constructor($scope, $state, courseResource, loginService) {
        this.$scope = $scope;
        this.$state = $state;
        this.courseResource = courseResource;
        this.loginStatus = loginService.status;
        this.currentCourse = {};
    }



    $onInit() {
        this.$scope.$watch(() => {
            return this.$state.params.id;
            },
            (newId, oldId) => {
                if(!newId) {
                    this.currentCourse = {};
                    return;
                }

                if(newId !== oldId) {
                    let courses = this.courseResource.getCourses();
                    for(let i = 0; i < courses.length; i++) {
                        if(courses[i]._id === newId) {
                            this.currentCourse = courses[i];
                            break;
                        }
                    }
                }
        });
    }
}

export default BreadcrumbsController;