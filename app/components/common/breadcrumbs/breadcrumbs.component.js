/**
 * Created by Oleg on 24/7/2016.
 */
import BreadcrumbsController from './breadcrumbs.controller';

const BreadcrumbsComponent = {
    controller: BreadcrumbsController,
    templateUrl: require('!ngtemplate?requireAngular!html!./breadcrumbs.html')
};

export default BreadcrumbsComponent;