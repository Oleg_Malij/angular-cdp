/**
 * Created by Oleg on 26/7/2016.
 */
'use strict';

class AuthorsService {
    /*@ngInject*/
    constructor() {
        this.authors = [
            {id: 1, name: 'Todd Motto'},
            {id: 2, name: 'Ivan Ivanov'},
            {id: 3, name: 'Petr Petrov'},
            {id: 4, name: 'Linus Torvalds'},
            {id: 5, name: 'John Papa'}
        ];
    }

    get() {
        return [].concat(this.authors);
    }

    processAuthors(from, to, selected) {
        selected.reduce((authors, next) => {
            from.forEach((v, k) => {
                if(isNaN(next)) {
                    if (v.id === next.id) {
                        authors.push(next);
                        from.splice(k, 1);
                    }
                }else {
                    if (v.id === next) {
                        authors.push(v);
                        from.splice(k, 1);
                    }
                }
            });

            return authors;
        }, to);
    }
}

export default AuthorsService;