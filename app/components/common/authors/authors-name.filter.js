/**
 * Created by Oleg on 28/7/2016.
 */
'use strict';
const AuthorsName = () => {
    return (data) => {
        return data.map((item) => {
            return item.name;
        }).join();
    };
};

export default AuthorsName;