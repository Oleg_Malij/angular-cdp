/**
 * Created by Oleg on 19/7/2016.
 */

const modalTemplate = require('!ngtemplate?requireAngular!html!./modal.html');

class ModalService {
    constructor($uibModal) {
        this.$uibModal = $uibModal;
        this.defaultOptions = {
            animation: false,
            backdrop: false,
            size: 'sm',
            controller: ModalCtrl,
            templateUrl: modalTemplate
        };
    }

    openModal(options) {
        Object.assign(this.defaultOptions, options);
        return this.$uibModal.open(this.defaultOptions).result;
    }
}

export default ModalService;

class ModalCtrl {
    constructor($scope, $uibModalInstance, params) {
        this.$uibModalInstance = $uibModalInstance;
        $scope.title = params.title;
        $scope.text = params.text;
        $scope.yesButton = params.yesButton;
        $scope.noButton = params.noButton;
    }
}
