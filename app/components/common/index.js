/**
 * Created by Oleg on 19/7/2016.
 */

import angular from 'npm/angular';
import modal from 'npm/angular-ui-bootstrap/src/modal';
import ModalService from './modal/modal.service';
import AuthorsService from './authors/authors.service';
import AuthorsNameFilter from './authors/authors-name.filter';
import BreadcrumbsComponent from './breadcrumbs/breadcrumbs.component';
import LogoComponent from './logo/logo.component';
/*@ngInject*/
const common = angular.module('common', [modal])
    .service('modalService', ModalService)
    .service('authorsService', AuthorsService)
    .component('breadcrumbs', BreadcrumbsComponent)
    .component('logo', LogoComponent)
    .filter('authorsName', AuthorsNameFilter)
    .name;

export default common;