/**
 * Created by Oleg on 24/7/2016.
 */
import LogoController from './logo.controller';

const LogoComponent = {
    controller: LogoController,
    templateUrl: require('!ngtemplate?requireAngular!html!./logo.html')
};

export default LogoComponent;