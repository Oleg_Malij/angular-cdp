/**
 * Created by Oleg on 18/6/2016.
 */
'use strict';

import angular from 'npm/angular';

import LoginComponent from './components/login';
import CourseComponent from './components/course';
import Common from './components/common';
import uiRouter from 'npm/angular-ui-router';
import ngResource from 'npm/angular-resource';

/*@ngInject*/
const root = angular.module('videoCatalog', [uiRouter, ngResource, Common, LoginComponent, CourseComponent])
    .config(function ($stateProvider, $transitionsProvider, $urlRouterProvider) {
        $transitionsProvider.onStart({
                to: 'login'
            },
            function($transition$, $state, loginService) {
                if(loginService.getLoginStatus()) {
                    return $state.go('courses');
                }
                return true;
            }
        );

        $transitionsProvider.onStart({
                to: 'courses'
            },
            function($transition$, $state, loginService) {
                if(!loginService.getLoginStatus()) {
                    return $state.go('login');
                }
                return true;
            }
        );

        $transitionsProvider.onStart({
                to: 'courseNew'
            },
            function($transition$, $state, loginService) {
                if(!loginService.getLoginStatus()) {
                    return $state.go('login');
                }
                return true;
            }
        );

        $stateProvider
            .state('login', {
                url: '/login',
                template: '<login>'
            })
            .state('courses', {
                url: '/courses',
                component: 'courses',
                resolve: {
                    courseData: (courseResource) => {
                        return courseResource.getAll().$promise.then((response) => response);
                    }
                }
            }).state('courseNew', {
                url: '/courses/:id',
                component: 'courseEdit',
                resolve: {
                    courseData: (courseResource, $stateParams) => {
                        if($stateParams.id === 'new') {
                            return Promise.resolve({});
                        }
                        return courseResource.get($stateParams.id).$promise.then((response) => response);
                    }
                }
            });

        $urlRouterProvider.otherwise('/login');
    })
    .run(['loginService', function (loginService) {
        loginService.setLoginStatus(loginService.getLoginStatus());
    }]);

export default root;